import { Component, OnInit } from '@angular/core';

// importancion de servicios
import { UsuariosService } from '../../services/usuarios.service';

//importacion de modelos
import { User } from '../../models/user';
import { Type } from '../../models/type';

@Component({
  selector: 'app-tabla',
  templateUrl: './tabla.component.html',
  styleUrls: ['./tabla.component.css']
})
export class TablaComponent implements OnInit {

  // varible de contenido de usuarios
  usuarios: User[];
  //variable de contenido de tipos
  tipos: Type[];
  //variable para nuevo usuario
  nuevoUsuario: User;
  editUsuario: User;

  // variable de edicion
  edit: Boolean = false;

  constructor(private usuariosservice: UsuariosService) { }

  ngOnInit(): void {
    this.usuarios = this.usuariosservice.getUsuarios();
    this.nuevoUsuario = this.usuariosservice.newUsuario();

    this.tipos = this.usuariosservice.getTipos();
  }
  
  // funcion para añadir un usuario
  agregarUsuario() {
    this.usuariosservice.setUsuario(this.nuevoUsuario);
    this.nuevoUsuario = this.usuariosservice.newUsuario();
  }

  // funcion para eliminar un usuario
  eliminarUsuario(id: number){
    this.usuariosservice.deleteUsuario(id);
  }

  // funcion para elminar un usuario
  editarUsuario(usuario: User){
    this.edit = true;
    // this.nuevoUsuario = usuario;
    this.nuevoUsuario = JSON.parse(JSON.stringify(usuario));
  }

  // funcion para guarda ediccion de usuario
  guardarEdicionUsuario(){
    this.usuariosservice.editUsuario(this.nuevoUsuario);
    this.nuevoUsuario = this.usuariosservice.newUsuario();
    this.edit = false;
  }

  cancelarEditarUsuario(){
    this.nuevoUsuario = this.usuariosservice.newUsuario();
    this.edit = false;
  }

}

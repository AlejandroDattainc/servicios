import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';

//componentes
import { TablaComponent } from './components/tabla/tabla.component';

// servicios
import { UsuariosService } from './services/usuarios.service';



@NgModule({
  declarations: [
    AppComponent,
    TablaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [
    UsuariosService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

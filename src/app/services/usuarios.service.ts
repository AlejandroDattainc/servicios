import { Injectable } from '@angular/core';

// importacion de modelos
import { User } from '../models/user';
import { Type } from '../models/type';

@Injectable()
export class UsuariosService {

    private usuarios: User[];
    private tipos: Type[];
    private ids: number;

    constructor() {
        // inicialziacion del objeto usuarios
        this.usuarios = [
            {
                id: 0,
                nombre: 'Alex',
                edad: 25,
                tipo: 'Basico'
            },
            {
                id: 1,
                nombre: 'Alejandro',
                edad: 25,
                tipo: 'Avanzado'
            }
        ];

        // inicializacion de ids global para el usuario
        this.ids = this.usuarios.length;

        //inicializacion del objeto tipos
        this.tipos = [
            {
                id: 0,
                tipo: 'Basico',
                precio: 200,
            },
            {
                id: 1,
                tipo: 'Avanzado',
                precio: 400,
            },
            {
                id: 2,
                tipo: 'Profesional',
                precio: 600,
            }
        ];
    }

    //metodo para obtener todos los usuarios
    getUsuarios() {
        return this.usuarios;
    }

    // metodo para añadir un nuevo usuario
    setUsuario(usuario: User) {
        this.ids += 1;
        this.usuarios.push(usuario);
        console.log(this.usuarios);
    }

    // metodo para inicializar un usuario
    newUsuario(): User {
        return {
            id: this.ids,
            nombre: '',
            edad: null,
            tipo: ''
        }
    }

    // metodo para eliminar un usuario
    deleteUsuario(id: number) {
        for (let index = 0; index < this.usuarios.length; index++) {
            if (id == this.usuarios[index].id) {
                this.usuarios.splice(index, 1);
                console.log("Borrado exitoso");
            }
        }
    }

    //metodo para editar usuario
    editUsuario(usuario: User) {
        for (let index = 0; index < this.usuarios.length; index++) {
            if (this.usuarios[index].id == usuario.id) {
                this.usuarios[index] = usuario;
                console.log("Editado exitoso")
                console.log(this.usuarios);
            }

        }
    }

    //metodo para obtener todos los tipos
    getTipos() {
        return this.tipos;
    }

}
